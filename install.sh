#!/usr/bin/env bash

# @TODO check for package manager and do distro-spcecific installation
sudo apt-get update
sudo apt-get -y install tmux pylint ruby inotify-tools git
sudo pip3 install asciinema
sudo gem install tmuxinator # hopefully your ruby version is new enough!!!
if ! command -v vim &> /dev/null; then
  # vim could not be found
  sudo apt-get install vim-gtk3
fi

# @TODO configure git name/email, read input from user

gitinit () {
  git init
  echo "*.swp" >> .gitignore
  echo "*.swo" >> .gitignore
  echo "*.tmp" >> .gitignore
  echo "*.sql" >> .gitignore
  echo "*.zip" >> .gitignore
  echo "*.tgz" >> .gitignore
  echo "*.tar.gz" >> .gitignore
  echo "node_modules" >> .gitignore
  echo "lrn" >> .gitignore
  echo "software_projects" >> .gitignore
  git add .gitignore
  git commit -m "Begin."
}

mkdir -p ~/code && cd ~/code

if [[ -d .git ]]; then
  echo "lrn" >> .gitignore # ignore lrn directory because it will contain numerous git repos
  echo "software_projects" >> .gitignore # ignore lrn directory because it will contain numerous git repos
else
  gitinit
fi


# configure $PATH
if [ -n "`$SHELL -c 'echo $ZSH_VERSION'`" ]; then
  echo 'export PATH="$HOME/code/bin:$PATH"' >> ~/.zshrc
  source ~/.zshrc
elif [ -n "`$SHELL -c 'echo $BASH_VERSION'`" ]; then
  echo 'export PATH="$HOME/code/bin:$PATH"' >> ~/.bashrc
  source ~/.bashrc
else
  echo "what shell are you using?"
  exit
fi

# install lrn
mkdir -p bin lrn software_projects && cd software_projects
git clone https://gitlab.com/lrn.software/lrn_launcher.git
ln -s ~/code/software_projects/lrn_launcher/lrn ~/code/bin/lrn

# download the lessons
cd ../lrn
#git clone https://gitlab.com/lrn.software/lrn_toolkit.git
git clone https://gitlab.com/lrn.software/parsing_text_files__python.git

echo "Installation complete!"
echo "Now you can run \`lrn\` to start a lesson!"
