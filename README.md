# `lrn` installation script
Use this script to install and configure `lrn`.

## To install
In your terminal:

```sh
git clone https://gitlab.com/lrn.software/lrn_installer.git
bash lrn_installer/install.sh
```

That will install the `lrn` program and download the lessons.

Once it's done, run `lrn` to start a lesson.

---

